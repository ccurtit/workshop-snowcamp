# 🏔 Workshop snowcamp : Exercice en solo

ℹ️ Pour ce workshop, vous pourrez faire l'ensemble des exercices depuis le WebIDE fournit par GitLab, ou via GitPod si vous préférez (ou sur votre poste si vraiment vous voulez).

## Connexion

Rendez-vous sur [🦊 Gitlab](gitlab.com) et au choix :
- connectez vous avec votre compte si vous en avez déjà un, 👍
- créez un compte avec le mode d'authentification de votre choix, 🆕
- levez la main si vous ne voulez pas vous créer un compte, nous vous fournirons un compte de test, 🙋🏻‍♂️

Une fois connecté, nous donner votre pseudo pour que nous vous inviter dans le groupe du [workshop](https://gitlab.com/gitlabuniversity/workshop-snowcamp-students).

## Premières étapes simples (issue, MR, ...)

Une fois connecté :
* Aller dans le groupe [gitlabuniversity/workshop-snowcamp-students](https://gitlab.com/gitlabuniversity/workshop-snowcamp-students)
* Créer un nouveau projet simple aka "Blank project"
    * Il existe des projets templates fournis par défaut par Gitlab, vous pouvez également en créer facilement 👉 [🔗 Official documentation](https://docs.gitlab.com/ee/user/admin_area/custom_project_templates)
* Créer une première [issue](https://docs.gitlab.com/ee/user/project/issues/) "📃 Update README.md"
    * L'objectif de cette issue doit être d'ajouter un schéma [mermaid](https://mermaid.live)
* A partir de cette issue, créer une [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/) et une branche pour cette issue
* En utilisant le WebIDE fourni dans Gitlab, modifier le fichier `README.md` sur le branche de l'issue et commiter
* Enfin, accepter la MR pour que le code soit mergé sur la branche `main`

```
✅ Le fichier README.md sur main est à jour avec le schéma mermaid
✅ L'issue a été automatiquement fermée lorsque le merge est terminé
```

Si l'issue n'a pas été fermé, c'est qu'il vous a manqué l'indication `Closes #1` dans la description de la MR.
Normalement, cela est positionné automatiquement si vous créez la MR depuis l'issue.

## Instanciation de la CI

**NB:** Gitlab fournit une aide pour initialiser ce type de fichier via `Set up CI/CD`.

Dans le cadre de ce workshop, nous allons créer le fichier *from scratch* pour bien comprendre les différents éléments 💪

### Mon premier job

Toujours dans le même repository:
* ajouter un fichier `.gitlab-ci.yml`
* au sein de ce fichier:
    * ajouter un [stage](https://docs.gitlab.com/ee/ci/yaml/#stages) `👋_hello`
    * ajouter un [job](https://docs.gitlab.com/ee/ci/yaml/#job-keywords) `🌍_hello-world` dans le stage `👋_hello`
        * ce job doit affiché `👋 Hello Snowcamp 2023 🏔` dans les logs
    * commiter le fichier sur la branche `main` directement
* aller dans le menu `CI/CD > Pipelines` et constater qu'un [pipeline](https://docs.gitlab.com/ee/ci/pipelines/) s'est déclenché

```
✅ Le pipeline doit s'exécuter avec succès 🎉
```

### Utilisation des variables

Ensuite, toujours dans le fichier `gitlab-ci.yml` :
* remplacer `Snowcamp 2023 🏔` par une [variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) définie au niveau du projet via le menu `Settings > CI/CD > Variables` et modifier sa valeur avec une autre conférence par exemple `Volcamp 2023 🌋`
* modifier également le message pour qu'il affiche le nom du projet et la branche sur laquelle s'exécute le pipeline grâce aux [variables pré-définies](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
* commiter le fichier sur la branche `main` directement
* retourner dans le menu `CI/CD > Pipelines` et constater qu'un [pipeline](https://docs.gitlab.com/ee/ci/pipelines/) s'est de nouveau déclenché

```
✅ Le pipeline doit s'exécuter avec succès 🎉
✅ Dans le log du job doit être affiché :

     👋 Hello Volcamp 2023 🌋 from xxx_project on branch main
```

### Plusieurs stages

Il est possible au sein de la CI:
* d'avoir plusieurs stages
* de définir l'image docker utilisée pour exécuter le job. Cela peut être une image officielle du hub docker ou une image custom

Dans le fichier `.gitlab-ci.yml`:
* ajouter un nouveau stage `🎨_ascii-art`
* ajouter 3 jobs en lien avec ce stage:
    * `🐮_meuh` : en utilisant une image `ubuntu`, installer l'outil `cowsay` avant l'exécution du script du job ([💡 astuce](https://docs.gitlab.com/ee/ci/yaml/#before_script)) et afficher une vache dans le log du job
    * `🐴_my-little-poney` : en utilisant l'image [ponysay](https://github.com/erkin/ponysay), afficher un poney dans le log
    * `🦖_jurassic-park` :  en utilisant l'image `python`, installer [dinosay](https://github.com/MatteoGuadrini/dinosay) et afficher un dinosaure dans le log

```
✅ Le pipeline et les 4 jobs doivent s'exécuter avec succès 🎉
```

On peut voir que dans cet exemple, les jobs du stage `🎨_ascii-art` attendent que le stage `👋_hello` soit terminé, puis ils s'exécutent en parallèle sans ordre précis.

Dans cette étape, nous allons forcer l'ordre des jobs:
* `🐮_meuh` ne doit pas attendre que le stage `👋_hello` s'exécute
* `🐴_my-little-poney` doit attendre que `🌍_hello-world` soit terminé
* `🦖_jurassic-park` doit attendre que `🐴_my-little-poney` soit terminé

👉 [astuce](https://docs.gitlab.com/ee/ci/yaml/#needs)

On peut voir simplement les liens entre les jobs soit via l'option `Jobs dependencies` soit via l'onglet `Needs` dans la page d'exécution du pipeline.

```
✅ Le pipeline et les 4 jobs doivent s'exécuter avec succès et dans le bon ordre 🎉
```

### Gestion des règles

Toujours dans le même projet:
* créer une issue `📏 Set up rules`
* créer une MR et une branche pour cette issue
* sur la branche nouvellement créée, modifier le fichier `.gitlab-ci.yml` de manière à ce que le job `🐴_my-little-poney` ne s'exécute que sur la branche `main`
    * [💡 astuce](https://docs.gitlab.com/ee/ci/yaml/#rules)
* commiter sur la branche

```
✅ Le pipeline s'exécute correctement 🎉 et le job 🐴_my-little-poney ne s'exécute pas ✋
```

* via la MR, merger la branche sur `main`
* vérifier que le pipeline s'exécute

```
✅ Le pipeline s'exécute correctement 🎉 et le job 🐴_my-little-poney s'exécute sur la branche main 🤡
```

## Utilisation de la fonctionnalité Pages

Gitlab propose la fonctionnalité [Pages](https://docs.gitlab.com/ee/user/project/pages/) pour facilement exposé un site statique à partir de sources dans un repository.

### Ma première Pages

Toujours dans le groupe [gitlabuniversity/workshop-snowcamp-students](https://gitlab.com/gitlabuniversity/workshop-snowcamp-students)

* Créér un nouveau projet `xxx-pages`
* Dans ce nouveau projet
    * créer un répertoire `public` contenant un fichier `index.html` avec un petit bout de code HTML simple ([snippet](https://gitlab.com/gitlabuniversity/workshop-snowcamp/-/snippets/2478942))
    * créer un fichier `.gitlab-ci.yml` à la racine du projet
    * ajouter un stage `deploy` et un job `pages` pour ce stage
        * ceux sont 2 mots clés protégés en Gitlab CI
        * faire du répertoire `public` un [artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) du job
    * commiter sur `main`
    * aller voir le pipeline qui s'exécute

```
✅ Le pipeline doit s'exécuter avec succès 🎉
```

**NB:** *un job a été automatiquement ajouté `pages:deploy`, c'est celui-ci qui déploie le site statique*

Une fois le pipeline exécuté correctement:
* aller dans le menu `Settings > Pages`
* trouver l'url de votre site

```
✅ Le site est déployé et accessible 👍
```

**NB:** *nous avons créer un projet à part pour démontrer cette fonctionnalité, mais cela peut être inclus dans un projet existant*

### Mon premier environnement

Pour simplifier l'accès au site, on peut également le lier à un [environnement](https://docs.gitlab.com/ee/ci/environments/)

Dans le fichier `.gitlab-ci.yml`:
* ajouter le paramétrage nécessaire dans le job `pages` pour qu'un environnement soit créé avec le nom de la branche
* commiter
* une fois le pipeline exécuté avec succès, aller dans le menu `Deployments > Environments`

```
✅ Le site est déployé et accessible depuis le menu Environments avec le nom `main`
```

### Release du projet

On peut classiquement tagger notre branche `main` pour fixer une version. GitLab propose en complément la notion de [Release](https://docs.gitlab.com/ee/user/project/releases/) permettant d'avoir un package comprenant cette version.

Depuis la version [15.7](https://about.gitlab.com/releases/2022/12/22/gitlab-15-7-released/), une nouvelle CLI est disponbible : [glab](https://gitlab.com/gitlab-org/cli). Celle-ci permet d'intéragir facilement avec GitLab et entre autres de créer une release.

* Depuis le menu `Repository -> Tags`, créer un tag `v1.0`
* Dans le `.gitlab-ci.yml`, ajouter un job **📦_release-with-glab** dans le stage **release** pour créer une release à partir de ce tag
    * pour que glab fonctionne il faut d'abord [s'authentifier](https://gitlab.com/gitlab-org/cli#authentication) en utilisant un [access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) que l'on peut générer depuis le menu `Preferences -> Access Token`, accessible en cliquant sur son avatar en haut à droite. Il faut que le token ait les droits `api` & `write_repository`
    * Une fois authentifié, on peut alors générer sa release
* Générer une release avec comme titre `🏔 Snowcamp Release <nom_tag>`
* Il est surement préférable que le job de release ne s'exécute que lorsque l'on est sur un pipeline lié à un tag... Ajouter une `rules` dans ce sens.

💡 astuce : [snippet partiel](https://gitlab.com/gitlabuniversity/workshop-snowcamp/-/snippets/2488569) (il faut remplacer les `****`)

```
✅ La release apparait dans Deployments -> Release avec le titre 🏔 Snowcamp Release <nom_tag>
✅ Le job de release ne s'exécute que sur les pipelines liés à un tag
✅ Les pipelines de main & des tags s'exécutent correctement
```

### Badges décoratifs

On peut associer décorer le projet avec des [badges](https://docs.gitlab.com/ee/user/project/badges.html) visibles sur la page d'accueil du projet. Cela est pratique pour avoir des informations sur le statut du pipeline, la couverture de tests, la licence, ...

Depuis le menu `Settings -> General -> Badges`
* Ajouter le badge de statut du pipeline

```
✅ Le badge du statut du pipeline apparait sur la page d'accueil
```

## Un projet plus réaliste

Maintenant que nous avons les bases, nous allons utiliser un projet de démo que nous avons préparer pour découvrir des fonctionnalités plus avancées.

* Forker le projet [web-app](https://gitlab.com/gitlabuniversity/web-app) via le bouton `Fork`
    * préfixer le nom du projet par votre trigramme à la fois dans **project name** & **project slug**
* Regarder le `.gitlab-ci.yml`, il inclut:
    * des notions connues : rules, stage, environment
    * 2 nouvelles notions :
        * `include`: permet d'importer un autre fichier `yml` contenant des définitions de stages et de jobs ([doc officielle](https://docs.gitlab.com/ee/ci/yaml/#include))
        * `extends`: permet de surcharger un job déjà défini. Cela peut être un job  *abstrait* que l'on préfixe avec `.`  ([doc officielle](https://docs.gitlab.com/ee/ci/yaml/#extends))

    👉 Ces 2 notions sont pratiques lorsque l'on souhaite mettre en place une bibliothèque de jobs réutilisables et/ou normaliser ses pipelines, mais aussi pour découper les jobs en plusieurs fichiers pour que cela reste lisible lorsque l'on a des pipelines avec beaucoup de jobs.

    * déclencher un pipeline manuellement depuis `CI/CD > Pipelines` et le bouton `Run Pipeline`
    * vérifier que l'environnement est bien créé et que l'application est déployée en cliquant sur le bouton `Open`

```
✅ Le site est déployé et accessible depuis le menu Environments avec le nom `xxx-web-app-main`
```

Il serait intéressant de pouvoir avoir un environnement par Merge Request afin de valider des modifications avant de valider celle-ci.

### Review app

Pour cela, GitLab fournit le mécanisme de `review app` ([doc officielle](https://docs.gitlab.com/ee/ci/review_apps/)) qui sont des environnements dynamiques.

Le projet `web-app` contient déjà les mécanismes dans le CI pour gérer cela (via la variable `DYNAMIC_ENVIRONMENT_URL`, cf le fichier dans le répertoire `ci/pages/`).

Désormais:
* créer une branche `review-app` et la MR associée
* sur cette branche, modifier le fichier `main.go` dans le répertoire `website` (✋ pas besoin de connaitre le go pour faire la modification 😅)
* commiter

```
✅ Le pipeline s'exécute correctement
✅ Un environnement a été ajouté dans la liste
✅ Le site modifié est déployé à l'URL liée à l'environnement
✅ L'environnement est affiché dans la MR
```

* merger la MR dans `main`

```
✅ Le pipeline s'exécute correctement
✅ L'environnement de la MR a été supprimé
✅ Le site de la branche main est mis à jour
```

### Gestion des tests unitaires

Dans les vrais projets, nous avons tous des tests unitaires. GitLab permet d'avoir des [rapports sur les tests unitaires](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) à différents niveaux.

Dans le projet en cours:
* ajouter un fichier de tests unitaires (en JS par exemple, [snippet](https://gitlab.com/gitlabuniversity/workshop-snowcamp/-/snippets/2478952))
* ajouter un job dans `.gitlab-ci.yml` pour exécuter ces tests
    * ce job doit propduire un *artifact* avec le fichier de résultats des tests
    * GitLab fournit des mots clés pour reconnaitre des [rapports](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html)
    * [snippet](https://gitlab.com/gitlabuniversity/workshop-snowcamp/-/snippets/2478957) pour vous aider à démarrer avec le job
* commiter sur la branche `main`

```
✅ Le pipeline s'exécute correctement
✅ Dans l'onglet du pipeline, le nombre de tests unitaires est affiché et tous sont ok
```

* créer une branche et une MR `failing-test`
* sur cette branche, ajouter un nouveau test dans `tests.js` qui ne passe pas
* commiter

```
✋ Le pipeline est en erreur
✅ Dans la MR, le résumé des tests apparait avec le test en erreur
```

* corriger le test en erreur
* commiter

```
✅ Le pipeline s'exécute correctement
✅ Dans la MR, le résumé des tests apparait sans test en erreur
```
Vous avez perdu le job de test dans le pipeline de la merge request ?
Modifiez le code avec ceci:
```yaml
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
    - if: $CI_MERGE_REQUEST_IID
```

### Code quality

GitLab fournit également des analyseurs de code pour contrôler la qualité, en se basant sur l'outil [CodeClimate](https://codeclimate.com/).

Dans le projet en cours:
* configurer code climate pour go en utilisant le [snippet](https://gitlab.com/gitlabuniversity/workshop-snowcamp/-/snippets/2478962) fourni
    * il existe d'autres [analyzers](https://docs.gitlab.com/ee/ci/testing/code_quality.html#using-analysis-plugins) en fonction du langage de programmation utilisés
* ajouter un job pour contrôler la qualité du code go en utilisant les [mécanismes fournis par GitLab](https://docs.gitlab.com/ee/ci/testing/code_quality.html)
* commiter sur la branche `main`

```
✅ Le pipeline s'exécute correctement
```

On constate que le job `code-quality` met un certain à s'exécuter par défaut. GitLab permet via la notion de [label](https://docs.gitlab.com/ee/ci/yaml/#tags) de pouvoir typer les jobs. Cela sert en particulier pour que les jobs soient exécutés sur des runners en particulier, comme par exemple certains [shared runners](https://docs.gitlab.com/ee/ci/runners/) mis à disposition sur gitlab.com.

* modifier le job `code-quality` pour qu'il soit pris en charge par un [runner `medium`](https://docs.gitlab.com/ee/ci/runners/saas/linux_saas_runner.html)
* commiter sur la branche main

```
✅ Le pipeline s'exécute correctement
✅ Le job code-quality s'exécute plus rapidement
✅ Le job a été exécuté par un shared runner medium
```

On peut également avoir le rapport de Code Quality au sein d'une merge request.

* créer une branche & une MR `code-quality`
* modifier le code pour ajouter du code moche (par exemple du code dupliqué)
* commiter sur la branche

```
✅ Le pipeline s'exécute correctement
✅ Le panel Code Quality apparait dans la MR
```

## Un autre projet

Nous allons utiliser un autre projet demo `web-api` pour découvrir les fonctionnalités autour de la gestion de conteneurs.

### Packaging Docker

* forker le projet [web-api](https://gitlab.com/gitlabuniversity/web-api) via le bouton `Fork`
    * préfixer le nom du projet par votre trigramme à la fois dans **project name** & **project slug**
* ajouter un job `🐳_build-image` pour générer une image Docker et pour la publier dans la [container registry](https://docs.gitlab.com/ee/user/packages/container_registry/) du projet
    * la version de l'image doit être le sha court du commit ([astuce](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html))
    * une manière simple est d'utiliser [kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html), cela évite de faire du *Docker-in-Docker* ou *DinD*
* commiter sur la branche `main`

```
✅ Le pipeline s'exécute correctement
✅ Dans le menu Packages and registries, l'image apparait dans la Container Registry avec comme version le sha court du commit
```

Il n'est peut pas nécessaire de publier une image pour chaque commit.

* créer une branche (sans merge request) `no-docker`
* modifier le `.gitlab-ci.yml` pour que le job `🐳_build-image` ne s'exécute que sur la branche `main` et sur les merge request
    * ([astuce](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html))
* commiter sur la branche

```
✅ Le pipeline s'exécute correctement
✅ Le job 🐳_build-image ne s'exécute pas
```

* créer maintenant la merge request de cette branche

Un pipeline se déclenche automatiquement.

```
✅ Le pipeline s'exécute correctement
✅ Le job 🐳_build-image s'exécute
```

### Tester une API directement depuis la pipeline

GitLab fournit le mécanisme de [services](https://docs.gitlab.com/ee/ci/services/) pour faire tourner un container en parallèle du container du job. 
Cela permet de pouvoir utiliser des services annexes comme une BDD lors d'une série de tests.

On peut aussi utiliser ce mécanisme pour déployer un container créé lors du job `🐳_build-image` et ainsi faire des tests sur les API exposés par notre application.

* repasser sur la branche `main`
* modifier le `.gitlab-ci.yml` et ajouter un job `🛂_test-api` qui s'exécute **après** le job `🐳_build-image`. 
    * Ce job utilise via un *service* l'image construite à l'étape précédente.
    * Ce job appelle plusieurs fois l'url `http://webapp:8080/api/hello`
* commiter

```
✅ Le pipeline s'exécute correctement
```

### Contrôle de sécurité

L'utilisation de dépendances open-sources, la complexification de la construction des applications font que les vecteurs de failles de sécurité sont de plus en plus nombreux. Pour contrôler cela, il existe de nombreux analyzers sur le marché. GitLab fournit des [jobs CI](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks) avec les outils prépackagés.

A partir de la branche `main`:
* créer une branche `check-security` et sa MR associée
* à partir de cette branche, modifier le `.gitlab-ci.yml` afin d'activer les jobs d'analyze SAST **à partir des templates fournis par GitLab**
    * pour l'analyse SAST
    * pour la détection de secrets
    * pour l'analyse de container
* commiter

```
✅ Le pipeline s'exécute correctement et sans warning
✅ 4 nouveaux jobs apparaissent dans le pipeline :
_ container_scanning
_ nodejs-scan-sast
_ secret_detection
_ semgrep-sast
```

Ces jobs génèrent des rapports intéressants. Il serait pratique de pouvoir avoir les informations dans le rapport de MR. Malheureusement, cette fonctionnalité n'est disponible qu'en version *Ultimate* de GitLab...
Mais il existe un contournement via le mécanisme de [notes](https://docs.gitlab.com/ee/user/project/merge_requests/reviews/#add-a-new-comment) disponibles dans les MR et les [API](https://docs.gitlab.com/ee/api/api_resources.html) exposées par GitLab. Cela ne permet une intégration aussi avancée mais permet de remonter certaines informations dans la MR.

Pour cela, il faut ajouter un job qui parse les résultats des analyzers.

* ajouter le fichier [js](https://gitlab.com/gitlabuniversity/workshop-snowcamp/-/snippets/2480699) qui permet de parser les fichiers json à la racine du repository
* En utilisant ce [snippet](https://gitlab.com/gitlabuniversity/workshop-snowcamp/-/snippets/2480700), modifier le `.gitlab-ci.yml` pour que les résultats soient remontés dans la MR
    * *NB* : il faut peut-être modifier les jobs existants

On utilise dans ce dernier snippet, 2 notions disponibles :
* les fonctions préfixées par `.` et référencées par un `&`
    * `.my-function: &my-function``
* les fonctions peuvent être importées dans les jobs en appelant `*my-function`
    * Les méthodes définitions dans cette fonction peuvent être ensuite utilisées dans le job

```
✅ Le pipeline s'exécute correctement
✅ Les vulnérabilités remontent dans la MR
```

### Scheduled pipeline

Il peut être pratique également d'avoir des pipelines qui s'exécutent de manière périodique : par exemple le matin pour construire une image avec le cache de dépendances mis à jour pour accélérer le temps d'exécution de vos jobs (en particulier quand on fait du JS ou du Java 😇). Un autre exemple est l'utilisation de bot tel que [renovabot](https://github.com/renovatebot/renovate) / [dependabot](https://github.com/dependabot) pour scanner et détecter des potentielles nouvelles versions/failles sur des dépendances.

* depuis le menu GitLab, ajouter un [scheduled pipeline](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) qui s'exécute toutes les 5min
* sauvegarder

```
✅ Le pipeline s'exécute correctement
✅ Le pipeline s'exécute correctement de nouveau 5min plus tard
```

# Utiliser ses propres runners

GitLab fournit un grand nombre de shared runners, mais dans certains cas, il peut être pratique d'avoir un *private* runner pour exécuter des jobs nécessitant un environnement d'exécution particulier, manipulant des données sensibles, ...

Il existe plusieurs méthodes pour instancier un runner, voici le chemin vers la [documentation](https://docs.gitlab.com/runner/register/index.html), faites votre choix
    * Le paragraphe est marqué en *deprecated* mais le nouveau mode de fonctionnement n'est pas encore déployé
    * Le plus simple reste via Docker/Podman

* enregistrer un runner local avec le tag **local**
* modifier le `.gitlab-ci.yml` pour qu'un de vos jobs soit avec le tag **local**
* commiter

```
✅ Le pipeline s'exécute correctement
✅ Le job taggé s'exécute bien sur le nouveau runner
✅ Les autres jobs s'exécutent toujours sur les shared runners GitLab
```

### Utilisation de child pipelines

Dans certains cas de figures, il arrive qu'un projet nécessite de déclencher le pipeline d'un autre projet. Pour cela, GitLab propose le mécanisme des [child pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html).
Dans ce workshop, nous allons modéliser le cas où l'on souhaite déclencher le pipeline de notre projet `web-app` à chaque fois que le projet `web-api` exécute un pipeline avec succès.

* ajouter dans `.gitlab-ci.yml` la référence pour déclencher le pipeline de votre projet `xxx-web-app` créé précédemment
* commiter

```
✅ Le pipeline s'exécute correctement
✅ Depuis le pipeline de web-api, on voit bien le lien vers le projet web-app
✅ Un pipeline du projet web-app se déclenche bien
```

# Dynamic child pipeline

Pour aller encore plus loin dans l'adaptabilité de nos pipelines, il est possible de *générer* des jobs / pipelines dynamiquement et de le déclencher (ou [trigger](https://docs.gitlab.com/ee/ci/yaml/index.html#trigger)).
Cela peut être pratique lorsque l'on souhaite, par exemple, construire une application pour différentes architectures CPU, ou dans différents niveaux de compatibilités...

* dans `.gitlab-ci.yml`, ajouter un job *🥸_generate-mustache* qui génère un fichier `.yml` contenant nos jobs
    * vous pouvez vous inspirer de ce [snippet](https://gitlab.com/gitlabuniversity/workshop-snowcamp/-/snippets/2487701) utilisant l'outil `mustache` de templating
    * ⚠️ Penser à ajouter les fichiers `data.json` & `ci.template` à la racine du projet disponibles [ici](https://gitlab.com/gitlabuniversity/workshop-snowcamp/-/snippets/2487717)
* ajouter un autre job *👶_child-pipelines* qui utilise le fichier précédemment généré
    * astuce par [ici](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#trigger-a-dynamic-child-pipeline)
* commiter

```
✅ Le pipeline s'exécute correctement
✅ Un downstream job est généré
✅ Les 3 jobs générés s'exécutent correctement
```

On peut ainsi facilement, ajouter de nouveaux jobs dans un pipeline sans avoir à dupliquer les configurations yml pour chacune des nouvelles valeurs.

* Dans le fichier `data.json`, ajouter `"rebellion"` dans la liste des items
* commiter

```
✅ Le pipeline s'exécute correctement
✅ Un downstream job est généré
✅ Il y a maintenant 4 jobs générés
```
